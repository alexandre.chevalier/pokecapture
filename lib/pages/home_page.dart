import 'dart:math';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pokecapture/models/pokemon_model.dart';

import 'package:pokecapture/services/pokeapi_service.dart';
import 'package:pokecapture/pokeapi-models/pokemon.dart';
import 'package:pokecapture/pokeapi-models/pokemon_species.dart';
import 'package:pokecapture/widgets/pokemon_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _pokemonId = [1, 4, 7, 25][Random().nextInt(3)];

  PokemonModel _pokemon = PokemonModel.empty();

  List<PokemonModel> _captured = [];
  double _missedTextOpacity = 0;

  final PokeapiService pokeapi = PokeapiService(client: http.Client());

  void _reloadPokemon() {
    int id = Random().nextInt(151) + 1;
    _loadPokemon(id);
  }

  void _loadPokemon(int id) async {
    Pokemon pokemon = await pokeapi.fetchPokemon(id);
    PokemonSpecies pokemonSpecies = await pokeapi.fetchPokemonSpecies(id);
    // 2% de chances qu'il soit shiny
    bool isShiny = (Random().nextInt(100) + 1 <= 2);
    PokemonModel pokemonModel =
        PokemonModel.from(pokemon, pokemonSpecies).beingShiny(isShiny);
    setState(() {
      _pokemonId = id;
      _pokemon = pokemonModel;
    });
  }

  void _displayMissedMessage() {
    setState(() {
      _missedTextOpacity = 1;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadPokemon(_pokemonId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: Theme.of(context).primaryColor),
      body: Column(mainAxisSize: MainAxisSize.min, children: [
        Container(
          decoration: BoxDecoration(
              color: Colors.black.withOpacity(.1),
              borderRadius: const BorderRadius.all(Radius.circular(20))),
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
          height: 64,
          child: ListView(
              scrollDirection: Axis.horizontal,
              children: _captured
                  .map((p) =>
                      Image.network(p.isShiny ? p.frontShiny : p.frontDefault))
                  .toList()),
        ),
        Center(
          child: PokemonWidget(pokemon: _pokemon),
        ),
        ElevatedButton(
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.blue),
          ),
          onPressed: () {
            // Capture du Pokémon courant
            if (Random().nextInt(255) + 1 <= _pokemon.captureRate) {
              setState(() {
                _captured = [_pokemon, ..._captured];
                _missedTextOpacity = 0;
              });
            } else {
              _displayMissedMessage();
            }
            _reloadPokemon();
          },
          child: const Text(
            'Capturer',
            style: TextStyle(color: Colors.white),
          ),
        ),
        Opacity(
          opacity: 0, // TODO: _missedTextOpacity,
          child: Text("${_pokemon.frenchName} raté !", textScaleFactor: 2),
        )
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: _reloadPokemon,
        tooltip: 'Refresh',
        child: const Icon(Icons.refresh),
      ),
    );
  }
}
