// Modélisation d'ue Espèce Pokémon
// URL associée : https://pokeapi.co/api/v2/pokemon-species/<id>
class PokemonSpecies {
  int id = 0;
  String name = "";
  String frenchName = "";
  String color = "";
  int captureRate = 0;

  // Constructeur sans paramètre pour initialiser un objet vide
  PokemonSpecies.empty();

  // Constructeur par défaut
  PokemonSpecies({
    required this.id,
    required this.name,
    required this.frenchName,
    required this.color,
    required this.captureRate,
  });

  // Instancier un objet de cette classe
  // à partir d'un JSON désérialisé en dynamic
  factory PokemonSpecies.fromJson(Map<String, dynamic> json) {
    String name_fr = "";
    // Recherche du nom en français dans les sous-objets
    for (dynamic name in json['names']) {
      if (name["language"]["name"] == "fr") {
        name_fr = name["name"];
      }
    }
    return PokemonSpecies(
        id: json['id'] as int,
        name: json['name'] as String,
        frenchName: name_fr,
        color: json['color']['name'] as String,
        captureRate: json['capture_rate'] as int);
  }
}
