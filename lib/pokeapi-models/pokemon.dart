// Modélisation d'un Pokémon
// URL associée : https://pokeapi.co/api/v2/pokemon/<id>
class Pokemon {
  int id = 0;
  String name = "";
  String frontDefault = "";
  String frontShiny = "";

  // Constructeur sans paramètre pour initialiser un objet vide
  Pokemon.empty();

  // Constructeur par défaut
  Pokemon({
    required this.id,
    required this.name,
    required this.frontDefault,
    required this.frontShiny,
  });

  factory Pokemon.fromJson(Map<String, dynamic> json) {
    return Pokemon(
        id: json['id'] as int,
        name: json['name'] as String,
        frontDefault: json['sprites']['front_default'] as String,
        frontShiny: json['sprites']['front_shiny'] as String);
  }
}
