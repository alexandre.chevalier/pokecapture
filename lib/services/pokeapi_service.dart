import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../pokeapi-models/pokemon.dart';
import '../pokeapi-models/pokemon_species.dart';

// Service de récupération d'objets depuis la PokeApi
class PokeapiService {
  // Client http nécessaire aux appels HTTP
  final http.Client client;

  // Constructeur par défaut
  PokeapiService({required this.client});

  // Récupération d'un objet Pokemon
  Future<Pokemon> fetchPokemon(int id) async {
    String url = "https://pokeapi.co/api/v2/pokemon/$id";
    http.Response response = await client.get(Uri.parse(url));
    String responseBody = response.body;
    final parsed = jsonDecode(responseBody);
    return Pokemon.fromJson(parsed);
  }

  // Récupération d'un objet PokemonSpecies
  Future<PokemonSpecies> fetchPokemonSpecies(int id) async {
    String url = "https://pokeapi.co/api/v2/pokemon-species/$id";
    http.Response response = await client.get(Uri.parse(url));
    String responseBody = response.body;
    final parsed = jsonDecode(responseBody);
    return PokemonSpecies.fromJson(parsed);
  }
}
