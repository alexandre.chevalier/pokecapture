import 'package:flutter/material.dart';
import 'package:color/color.dart' as colorz;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:pokecapture/models/pokemon_model.dart';

class PokemonWidget extends StatefulWidget {
  final PokemonModel pokemon;

  const PokemonWidget({Key? key, required this.pokemon}) : super(key: key);

  @override
  State<PokemonWidget> createState() => _PokemonWidgetState();
}

class _PokemonWidgetState extends State<PokemonWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation _animation;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 600),
    );
    _animation =
        CurvedAnimation(parent: _animationController, curve: Curves.linear);

    // _animationController.forward();
    _animationController.repeat(reverse: true);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    int pokemonId = widget.pokemon.id;
    // Taux de capture en %
    int captureChance = ((widget.pokemon.captureRate / 255) * 100).round();

    colorz.RgbColor color = colorz.RgbColor.namedColors[widget.pokemon.color] ??
        colorz.RgbColor.name("black");
    Color pokemonColor =
        Color.fromRGBO(color.r.toInt(), color.g.toInt(), color.b.toInt(), 1);

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
            "${widget.pokemon.frenchName} ${widget.pokemon.isShiny ? 'shiny' : ''}",
            style: Theme.of(context).textTheme.headline4),
        Text("Chances de capture : $captureChance%",
            style: Theme.of(context).textTheme.headline5),
        SizedBox(
          width: 300,
          height: 300,
          child: CachedNetworkImage(
            fadeInCurve: Curves.ease,
            imageBuilder: (context, imageProvider) => Container(
              alignment: Alignment.center,
              child: widget.pokemon.name.isNotEmpty
                  ? RotationTransition(
                      turns: Tween(begin: -0.02, end: 0.02)
                          .animate(_animationController),
                      child: Image(
                          image: imageProvider,
                          width: 300,
                          height: 300,
                          fit: BoxFit.fill),
                    )
                  : const Icon(Icons.hourglass_empty),
            ),
            placeholder: (context, url) => CircularProgressIndicator(
              strokeWidth: 8,
              color: pokemonColor,
            ),
            imageUrl: widget.pokemon.isShiny
                ? widget.pokemon.frontShiny
                : widget.pokemon.frontDefault,
          ),
        ),
        const Text('Pokémon'),
        Text(
          'N°$pokemonId',
          style: Theme.of(context).textTheme.headline4,
        ),
        if (widget.pokemon.isShiny)
          Padding(
            padding: const EdgeInsets.all(20),
            child: Text("Il est shiny !",
                style: Theme.of(context).textTheme.headline5),
          )
      ],
    );
  }
}
