import 'package:pokecapture/pokeapi-models/pokemon.dart';
import 'package:pokecapture/pokeapi-models/pokemon_species.dart';

// Modélisation d'un Pokémon pour l'affichage
class PokemonModel {
  int id = 0;
  String name = "";
  String frontDefault = "";
  String frontShiny = "";
  String frenchName = "";
  String color = "";
  int captureRate = 0;
  bool isShiny = false;

  // Constructeur sans paramètre pour initialiser un objet vide
  PokemonModel.empty();

  // Constructeur par défaut
  PokemonModel({
    required this.id,
    required this.name,
    required this.frontDefault,
    required this.frontShiny,
    required this.frenchName,
    required this.color,
    required this.captureRate,
    this.isShiny = false,
  });

  factory PokemonModel.from(Pokemon pokemon, PokemonSpecies pokemonSpecies) {
    return PokemonModel(
        id: pokemon.id,
        name: pokemon.name,
        frenchName: pokemonSpecies.frenchName,
        frontDefault: pokemon.frontDefault,
        frontShiny: pokemon.frontShiny,
        captureRate: pokemonSpecies.captureRate,
        color: pokemonSpecies.color);
  }

  PokemonModel beingShiny(bool value) {
    isShiny = value;
    return this;
  }
}
